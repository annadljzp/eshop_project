package com.sun.webmanage.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.commons.utils.ActionUtils;
import com.sun.exception.DaoException;
import com.sun.webmanage.model.TbItem;
import com.sun.webmanage.model.TbItemDesc;
import com.sun.webmanage.model.TbItemParamItem;
import com.sun.webmanage.service.FileUploadService;
import com.sun.webmanage.service.TbItemService;

/**
 * @author sunhongmin
 * 商品
 */
@Controller
public class TbItemController {
	
	@Resource
	private TbItemService tbItemService;
	@Resource
	private FileUploadService fileUploadService;
	
	private final static Logger LOGGER = LoggerFactory.getLogger(TbItemController.class);
	
	@ResponseBody
	@RequestMapping("/item/list")
	public EasyUIDataGrid listTbItem(Integer page,Integer rows,String sort,String order){
		EasyUIDataGrid listdata = tbItemService.listTbItemPage(page, rows,sort,order);
		LOGGER.info("128");
		return listdata;
	}
	
	@ResponseBody
	@RequestMapping("/rest/item/delete")
	public Map<String,Object> delete(String ids){
		try {
			if(tbItemService.editStatusTbItem(ids,"3")){
			return ActionUtils.ajaxSuccess("删除成功", null);	
			}
		} catch (DaoException e) {
			e.printStackTrace();
			LOGGER.error("delete error==="+e.getMessage());
			return ActionUtils.ajaxFail("删除失败", "提示：系统错误");
		}
		return ActionUtils.ajaxFail("删除失败", null);
	}
	
	@ResponseBody
	@RequestMapping("/rest/item/instock")
	public Map<String,Object> instock(String ids){
		try {
			if(tbItemService.editStatusTbItem(ids,"2")){
				return ActionUtils.ajaxSuccess("下架成功", null);	
			}
		} catch (DaoException e) {
			e.printStackTrace();
			LOGGER.error("instock error==="+e.getMessage());
			return ActionUtils.ajaxFail("下架失败", "提示：系统错误");
		}
		return ActionUtils.ajaxFail("下架失败", null);
	}
	
	@ResponseBody
	@RequestMapping("/rest/item/reshelf")
	public Map<String,Object> reshelf(String ids){
		try {
			if(tbItemService.editStatusTbItem(ids,"1")){
				return ActionUtils.ajaxSuccess("上架成功", null);	
			}
		} catch (DaoException e) {
			e.printStackTrace();
			LOGGER.error("reshelf error==="+e.getMessage());
			return ActionUtils.ajaxFail("上架失败", "提示：系统错误");
		}
		return ActionUtils.ajaxFail("上架失败", null);
	}
	
	/*@ResponseBody
	@RequestMapping("/upload")
	public Map<String,Object> fileUpload(MultipartFile file){
		String url = fileUploadService.fileUpload(file);
		if(url!=null && !url.equals("")){
			return ActionUtils.ajaxSuccess("上传成功", null);
		}else{
			return ActionUtils.ajaxFail("上传失败", null);
		}
	}*/
	
	@ResponseBody
	@RequestMapping("/pic/upload")
	public Map<String,Object> picUpload(MultipartFile uploadFile){
		Map<String,Object> retmap = new HashMap<>();
		String imgSucUrl = fileUploadService.fileUpload(uploadFile);
		if(imgSucUrl!=null && !imgSucUrl.equals("")){
			retmap.put("error", 0);
			retmap.put("url", imgSucUrl);
		}else{
			retmap.put("error", 1);
			retmap.put("message", "文件上传失败");
		}
		return retmap;
	}
	
	@ResponseBody
	@RequestMapping("/item/save")
	public Map<String,Object> save(TbItem tbItem,String desc,String itemParams){
		Map<String, Object> result = null;
		try {
			result = tbItemService.saveTbItemRelatoinInfo(tbItem, desc,itemParams);
			return result;
		} catch (DaoException e) {
			e.printStackTrace();
			LOGGER.error("save error==="+e.getMessage());
			return ActionUtils.ajaxFail("商品新增失败", "提示：系统错误");
		}
	}
	
	/**
	 * 商品描述
	 * @param item_id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/rest/item/query/item/desc/{item_id}")
	public Map<String,Object> loadItemDesc(@PathVariable Long item_id){
		TbItemDesc itemDesc = tbItemService.loadTbItemDescByItemId(item_id);
		if(itemDesc!=null){
			return ActionUtils.ajaxSuccess("", itemDesc);
		}else{
			return ActionUtils.ajaxFail("", "");
		}
	}
	
	/**
	 * 商品规格参数
	 * @param item_id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/rest/item/param/item/query/{item_id}")
	public Map<String,Object> loadItemParam(@PathVariable Long item_id){
		TbItemParamItem itemParam = tbItemService.loadTbItemParamItemByItemId(item_id);
		if(itemParam!=null){
			return ActionUtils.ajaxSuccess("", itemParam);
		}else{
			return ActionUtils.ajaxFail("", "");
		}
	}
	
	@RequestMapping("/rest/page/item-edit")
	public String editItemPage(){
		return "item-edit";
	}
	
	@ResponseBody
	@RequestMapping("/rest/item/update")
	public Map<String,Object> update(TbItem tbItem,String desc,String itemParams,Long itemParamId){
		return tbItemService.updateTbItemRelationInfo(tbItem, desc, itemParams, itemParamId);
	}

}
