package com.sun.webmanage.service;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.dubbo.webmanage.service.TbContentDubboService;
import com.sun.redis.dao.RedisDao;
import com.sun.webmanage.model.TbContent;

@Service("TbContentService")
public class TbContentServiceImpl implements TbContentService{

	@Reference
	private TbContentDubboService tbContentDubboService;
	
	@Resource
	private RedisDao redisDao;
	@Value("portal.cache.big.advertisement")
	private String key;

	@Override
	public EasyUIDataGrid listTbContentPage(int page, int rows, long cid) {
		return tbContentDubboService.listTbContentPage(page, rows, cid);
	}

	@Override
	public boolean saveTbContent(TbContent tbContent) {
		Date now = new Date();
		tbContent.setCreated(now);
		tbContent.setUpdated(now);
		Long id = tbContentDubboService.saveTbContent(tbContent);
		if(id!=null){
			/*if(redisClusterDubboService.exists(key)){
				//刷新redis缓存
				String jsondata = redisClusterDubboService.getKey(key);
				List<BigAdvertisement> listdata = JsonUtils.jsonToList(jsondata, BigAdvertisement.class);
				listdata.remove(listdata.size()-1);
				BigAdvertisement ad = new BigAdvertisement();
				ad.setId(id);
				ad.setSrc(tbContent.getPic());
				ad.setSrcB(tbContent.getPic2());
				ad.setHref(tbContent.getUrl());
				ad.setAlt(tbContent.getTitle());
				listdata.add(0, ad);
				redisClusterDubboService.setKey(key, JsonUtils.objectToJson(listdata));
			}*/
			return true;
		}
		
			return false;
		
	}

	@Override
	public boolean editTbContent(TbContent tbContent) {
		Date now = new Date();
		tbContent.setUpdated(now);
		boolean status = tbContentDubboService.editTbContent(tbContent);
		if(status){
			/*if(redisClusterDubboService.exists(key)){
				String jsondata = redisClusterDubboService.getKey(key);
				List<BigAdvertisement> listdata = JsonUtils.jsonToList(jsondata, BigAdvertisement.class);
				for(BigAdvertisement ad:listdata){
					if(ad.getId() == tbContent.getId()){
						ad.setSrc(tbContent.getPic());
						ad.setSrcB(tbContent.getPic2());
						ad.setHref(tbContent.getUrl());
						ad.setAlt(tbContent.getTitle());
					}
				}
			}*/
			return true;
		}
		
		return false;
	}

	@Override
	public boolean delTbContent(String ids) {
		String[] id_str = ids.split(",");
		boolean flag = true;
		for(String id:id_str){
			if(!tbContentDubboService.deleteTbContent(Long.parseLong(id))){
				flag = false;
			}
		}
		
		/*if(flag){
			if(redisClusterDubboService.exists(key)){
				List<TbContent> listdata = tbContentDubboService.listTbContentLimit(89,6, true);
				List<BigAdvertisement> listAd = new ArrayList<>();
				for(TbContent tb:listdata){
					BigAdvertisement ad = new BigAdvertisement();
					ad.setId(tb.getId());
					ad.setSrc(tb.getPic());
					ad.setHref(tb.getUrl());
					ad.setAlt(tb.getTitle());
					listAd.add(ad);
				}
				String jsonstr = JsonUtils.objectToJson(listAd);
				redisClusterDubboService.setKey(key, jsonstr);
			}
		}*/
		
		return flag;
	}
}
